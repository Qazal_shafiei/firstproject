<html>

<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <title>افزودن غذا</title>
    </link>
</head>

<body dir="rtl" style="text-align:right;">
<div style="padding: 50px;text-align:right;">
        <nav class="navbar navbar-expand-sm">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="{{route ('Hallcreate')}}" class="btn btn-primary">سالن جدید</a>
                </li>
                <li class="nav-item">
                    <a href="{{route ('create')}}" class="btn btn-primary">دسته بندی جدید</a>
                </li>
               
                <form action="{{ route('logout') }}" method="POST">
                @csrf
                <button type="submit" class="btn btn-danger">خروج</button>
                </form>
            </ul>
        </nav>
    </div>
    <div class="container">
    <!-- inclouding errors -->
        @include('layouts.messages')
        <form action="{{route('Foodstore')}}" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">نام غذا:</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name">

                <label for="title">قیمت:</label>
                <input type="text" class="form-control @error('price') is-invalid @enderror" name="price">

                <label for="title">تعداد:</label>
                <input type="text" class="form-control @error('count') is-invalid @enderror" name="count">

                <label for="title">انتخاب دسته بندی:</label>
                <div id="output"></div>
                <select class="chosen-select" name="categories[]" multiple>
                    @foreach ($categories as $cat_id => $cat_title)
                        <option value="{{ $cat_id }}">{{ $cat_title }}</option>
                    @endforeach
                </select>

              
            </div>
            @error('title')
            <div class="alert alert-danger" role="alert">
                <strong>{{$message}}</strong>
            </div>
            @enderror
            <div class="form-group">
                <button type="submit" class="btn btn-success">ثبت</button>
            </div>
        </form>


    </div>
</body>

</html>