<html>
<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <title>صفحه سالن ها </title>
    </link>
</head>
<body dir="rtl" style="text-align:right;">
<div style="padding: 50px;text-align:right;">
        <nav class="navbar navbar-expand-sm">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="{{route('Foodcreate')}}" class="btn btn-warning">افزودن غذا</a>
                </li>
                <form action="{{ route('logout') }}" method="POST">
                @csrf
                <button type="submit" class="btn btn-danger">خروج</button>
                </form>
            </ul>
        </nav>
    </div>
    <div class="container">
        <div class="d-flex justify-content-center">
        
            <table class="table">
                <thead>
                    <tr>
                        <td> شناسه</td>
                        <td> نام غذا</td>
                        <td> قیمت</td>
                        <td> تعداد</td>
                        <td> ویرایش</td>
                        <td> حذف</td>
                    </tr>
                </thead>

                <body>
                    @foreach($foods as $food)
                    <tr>
                        <td> {{$food->id}} </td>
                        <td><a href=""> {{$food->name}} </a></td>
                        <td> {{$food->price}}</td>
                        <td> {{$food->count}} </td>
                        <td> <a href="" class="btn btn-primary">ویرایش</a> </td>
                        <td> <a href="" class="btn btn-danger" onclick="return confirm('آیتم مورد نظر حذف شود؟');">حذف</a> </td>
                    </tr>
                    @endforeach
                </body>
            </table>
        </div>
    </div>
</body>
</html>