<html>

<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <title>صفحه دسته بندی ها</title>
    </link>
</head>

<body dir="rtl" style="text-align:right;">
<div style="padding: 50px;text-align:right;">
        <nav class="navbar navbar-expand-sm">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="{{route ('create')}}" class="btn btn-primary">دسته بندی جدید</a>
                </li>
                <li class="nav-item">
                    <a href="{{route('Food')}}" class="btn btn-warning">لیست غذا</a>
                </li>
                <form action="{{ route('logout') }}" method="POST">
                @csrf
                <button type="submit" class="btn btn-danger">خروج</button>
                </form>
            </ul>
        </nav>
    </div>
   
    <div class="container">
        <div class="d-flex justify-content-center">

            <table class="table">
                <thead>
                    <tr>
                        <td> شناسه</td>
                        <td> نام دسته بندی</td>
                        <td> ویرایش</td>
                        <td> حذف</td>
                    </tr>
                </thead>

                <body>
                    @foreach($category as $cat)
                    <tr>
                        <td> {{$cat->id}} </td>
                        <td><a href=""> {{$cat->title}} </a></td>
                        <td> <a href="{{route('edit', $cat->id)}}" class="btn btn-primary">ویرایش</a> </td>
                        <td> <a href="{{route('destroy', $cat->id)}}" class="btn btn-danger" onclick="return confirm('آیتم مورد نظر حذف شود؟');">حذف</a> </td>
                    </tr>
                    @endforeach
                </body>
            </table>
        </div>
    </div>
</body>

</html>