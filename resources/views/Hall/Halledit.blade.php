<html>

<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <title>دسته بندی جدید</title>
    </link>
</head>

<body dir="rtl" style="text-align:right;">
<div style="padding: 50px;text-align:right;">
        <nav class="navbar navbar-expand-sm">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="{{route ('Hallcreate')}}" class="btn btn-primary">سالن جدید</a>
                </li>
                <form action="{{ route('logout') }}" method="POST">
                @csrf
                <button type="submit" class="btn btn-danger">خروج</button>
                </form>
            </ul>
        </nav>
    </div>
    <div class="container">
    <!-- inclouding errors -->
        @include('layouts.messages')
        <form action="{{route('Hallupdate', $hall->id)}}" method="POST">
        @method('put')
            @csrf
            <div class="form-group">
                <label for="title">نام سالن:</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" name="name" value="{{$hall->name}}">

                <label for="title">طبقه:</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" name="floor" value="{{$hall->Floor}}">

                <label for="title">ظرفیت:</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" name="capacity" value="{{$hall->capacity}}">
            </div>
            @error('title')
            <div class="alert alert-danger" role="alert">
                <strong>{{$message}}</strong>
            </div>
            @enderror
            <div class="form-group">
                <button type="submit" class="btn btn-success">ثبت</button>
            </div>
        </form>


    </div>
</body>

</html>