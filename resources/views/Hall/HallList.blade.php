<html>
<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <title>صفحه سالن ها </title>
    </link>
</head>
<body dir="rtl" style="text-align:right;">
<div style="padding: 50px;text-align:right;">
        <nav class="navbar navbar-expand-sm">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="{{route ('Hallcreate')}}" class="btn btn-primary">سالن جدید</a>
                </li>
                <form action="{{ route('logout') }}" method="POST">
                @csrf
                <button type="submit" class="btn btn-danger">خروج</button>
                </form>
            </ul>
        </nav>
    </div>
    <div class="container">
        <div class="d-flex justify-content-center">
        
            <table class="table">
                <thead>
                    <tr>
                        <td> شناسه</td>
                        <td> نام سالن</td>
                        <td> طبقه</td>
                        <td> ظرفیت</td>
                        <td> ویرایش</td>
                        <td> حذف</td>
                    </tr>
                </thead>

                <body>
                    @foreach($halls as $hall)
                    <tr>
                        <td> {{$hall->id}} </td>
                        <td><a href=""> {{$hall->name}} </a></td>
                        <td> {{$hall->Floor}}</td>
                        <td> {{$hall->capacity}} </td>
                        <td> <a href="{{route('Halledit', $hall->id)}}" class="btn btn-primary">ویرایش</a> </td>
                        <td> <a href="{{route('Halldestroy', $hall->id)}}" class="btn btn-danger" onclick="return confirm('آیتم مورد نظر حذف شود؟');">حذف</a> </td>
                    </tr>
                    @endforeach
                </body>
            </table>
        </div>
    </div>
</body>
</html>