@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('پنل مدیریت') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    

                    <a href="{{Route('category')}}" class="btn btn-primary">دسته بندی غذا ها</a></br></br>
                    <a href="{{Route('Hall')}}" class="btn btn-primary">لیست سالن ها</a></br></br>
                    <a href="{{Route('category')}}" class="btn btn-primary">لیست سفارشات</a></br></br>
                    <a href="{{Route('Food')}}" class="btn btn-primary">لیست غذا</a></br></br>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection