<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\HallController;
use App\Http\Controllers\FoodController;
use App\Models\category;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Auth::routes();
//Categories Routes
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/category', [CategoryController::class, 'index'])->name('category');
Route::get('/category/create', [CategoryController::class, 'create'])->name('create');
Route::get('/', [CategoryController::class, 'show'])->name('show');
Route::post('/category/store', [categoryController::class, 'store'])->name('store')->middleware('auth');
Route::get('/category/edit/{category}', [categoryController::class, 'edit'])->name('edit')->middleware('auth');
Route::put('/category/update/{category}', [categoryController::class, 'update'])->name('update')->middleware('auth');
Route::get('/category/destroy/{category}', [categoryController::class, 'destroy'])->name('destroy')->middleware('auth');
//

//Halls Routs
Route::get('/Hall', [HallController::class, 'index'])->name('Hall');
Route::get('/Hall/create', [HallController::class, 'create'])->name('Hallcreate')->middleware('auth');
Route::put('/Hall/update/{hall}', [HallController::class, 'update'])->name('Hallupdate')->middleware('auth');
Route::get('/Hall/edit/{hall}', [HallController::class, 'edit'])->name('Halledit')->middleware('auth');
Route::post('/Hall/store', [HallController::class, 'store'])->name('Hallstore')->middleware('auth');
Route::get('/Hall/destroy/{hall}', [HallController::class, 'destroy'])->name('Halldestroy')->middleware('auth');
//

//Foods Routs
Route::get('/Food', [FoodController::class, 'index'])->name('Food');
Route::get('/Food/create', [FoodController::class, 'create'])->name('Foodcreate')->middleware('auth');
Route::put('/Food/update/{food}', [FoodController::class, 'update'])->name('Foodupdate')->middleware('auth');
Route::get('/Food/edit/{food}', [FoodController::class, 'edit'])->name('Foodedit')->middleware('auth');
Route::post('/Food/store', [FoodController::class, 'store'])->name('Foodstore')->middleware('auth');
Route::get('/Food/destroy/{food}', [FoodController::class, 'destroy'])->name('Fooddestroy')->middleware('auth');
//
