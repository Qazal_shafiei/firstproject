<?php

namespace App\Http\Controllers;

use App\Models\category;
use App\Models\food;
use Exception;
use Illuminate\Http\Request;

class FoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $foods = food::all();
        return view('Food.FoodList', compact('foods'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = category::all()->pluck('title', 'id');
        return view('Food.Foodcreate', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $messages = [
            'name.required' => 'فیلد عنوان را وارد کنید',
            'name.unique' => 'فیلد عنوان تکراری است'
        ];

        $validatedData = $request->validate([
            'name' => 'required|unique:food'
        ], $messages);

        $food = new food();
        try {
            $food = $food->create($request->all());
            $food->categories()->attach($request->categories);
            //$cat->save();   
        } catch (Exception $exception) {
            return redirect(route('Food'))->with('warning', $exception->getCode());
        }
        $msg = "عملیات با موفقیت انجام شد.";
        return redirect(route('Food'))->with('success', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\food  $food
     * @return \Illuminate\Http\Response
     */
    public function show(food $food)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\food  $food
     * @return \Illuminate\Http\Response
     */
    public function edit(food $food)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\food  $food
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, food $food)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\food  $food
     * @return \Illuminate\Http\Response
     */
    public function destroy(food $food)
    {
        //
    }
}
