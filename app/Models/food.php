<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class food extends Model
{
    use HasFactory;
    public $fillable = ['name','count', 'price'];
   
    //یه غذا میتونه در چندین سالن سرو بشه
    public function categories()
    {
        return $this->belongsToMany(category::class);
    }
}
